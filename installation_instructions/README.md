# Installation Instructions

This directory contains instructions to install the software needed for the course.  There are three `.ipynb` Jupyter notebook files that need to be maintained.

Files to edit:

* `index.ipynb`
* `windows/installation_instructions.ipynb`
* `macintosh/installation_instructions.ipynb`

After editing/changing these notebook files, export the notebook from Jupyter lab as a `.html` file and push all of those changes to this online repo and merge with the main branch.  After that inform the instructor who can pull and update the course web page with those html files.

In addition, this directory contains the `projects_landing` folder, which contains the `index.ipynb` file that explains to students how to download projects for the course and setup and do the projects.
