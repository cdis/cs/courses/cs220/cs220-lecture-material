# Project 12 (P12) grading rubric

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Displaying excessive irrelevant information (-3)
- Required outputs not visible/did not save the notebook file prior to running the cell containing "export" (-3)
- Used concepts/modules (like csv.DictReader, numpy) not covered in class yet (built-in functions that you have been introduced to can be used) (-3)
- `import` statements are not mentioned in the required cell at the top of the notebook or used additional import statements beyond those that are stated in the directions (-3)

### Question specific guidelines:

- `download` (6)
	- Downloading the file even if it already exists (-5)
	- Function is defined more than once (-1)

- Q1 (3)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)

- Q2 (3)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)

- Q3 (3)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)

- Q4 (4)
	- Recomputed variable defined in Question 3 (-1)
	- Conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- Q5 (3)
	- Recomputed variable defined in Question 3 (-1)
	- Conditional statements or loops are used (-1)

- Q6 (3)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)

- Q7 (4)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- Q8 (4)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)

- Q9 (5)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)
	- Incorrect logic is used to answer (-1)

- Q10 (5)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)
	- Incorrect logic is used to answer (-1)

- Q11 (5)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)
	- Incorrect logic is used to answer (-2)

- `institutions_df` (4)
	- Data structure is defined incorrectly (-3)
	- Data structure is defined more than once (-1)

- Q12 (4)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)

- Q13 (5)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)
	- `.loc` is used on a pandas DataFrame to hardcode an index (-1)
	- Incorrect logic is used to answer (-1)

- Q14 (4)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)

- Q15 (5)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)
	- Incorrect logic is used to answer (-1)

- Q16 (5)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-1)
	- Incorrect logic is used to answer (-1)

- Q17 (5)
	- Required data structure is not used (-1)
	- Conditional statements or loops are used (-2)
	- Incorrect logic is used to answer (-1)

- Q18 (5)
	- Hardcoded header (-1)
	- Incorrect logic is used to answer (-2)

- `parse_html` (6)
	- Function does not typecast based on columns (-1)
	- Function does not assign None for missing data (-1)
	- Function is defined more than once (-1)
	- Function logic is incorrect (-3)

- Q19 (4)
	- Required function is not used (-2)
	- Incorrect logic is used to answer (-1)

- Q20 (5)
	- Required function is not used (-3)
